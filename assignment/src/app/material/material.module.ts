import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox'; 
import {MatTableModule} from '@angular/material/table'; 

import {MatPaginatorModule} from '@angular/material/paginator'; 

const commonMaterialComponents = [
  MatButtonModule,
  MatIconModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatInputModule,
  MatCardModule,
  MatCheckboxModule,
  MatTableModule,
  MatPaginatorModule
]

@NgModule({
  imports: [commonMaterialComponents],
  exports: [commonMaterialComponents]
})
export class MaterialModule { }
