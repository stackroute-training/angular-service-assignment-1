import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { Note } from '../model';

@Component({
  selector: 'app-crud-operations',
  templateUrl: './crud-operations.component.html',
  styleUrls: ['./crud-operations.component.css']
})
export class CrudOperationsComponent implements OnInit {

  note:Note;
  notes:Note[];
  
  constructor(private apiSer:ApiService) { 
     
  }

  ngOnInit(): void {
    this.fetch();
  }

  search(){
    this.apiSer.getByID(this.note.id).subscribe(res =>{
      this.note = res;
      this.notes = [this.note]
    },(err)=>{
      console.log(err);
    })
  }

  fetch(){
    this.note=new Note(); 
    this.apiSer.getAll().subscribe(response=>{
      this.notes=response;
    },(error)=>{
      console.log(error);
    })
  }

  add(){
    this.apiSer.PostData(this.note).subscribe(res=>{
      if(res){
        alert("Added Succesfully")
      }
    },(er)=>console.log(er))
  }

  delete(){
    this.apiSer.deleteByID(this.note.id).subscribe(res=>{
      if(res){
        alert("Deleted Succesfully")
      }
    },(er)=>console.log(er))
  }

  edit(){
    this.apiSer.putByID(this.note).subscribe(res=>{
      this.note = res
     console.log(this.note);
    },(err)=>console.log(err))
  }

  
}
