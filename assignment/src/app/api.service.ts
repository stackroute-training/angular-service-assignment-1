import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Note } from './model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  api_path:string = 'http://localhost:3000/todo';
  constructor(private http:HttpClient) { }

  getAll():Observable<Note[]>{
    return this.http.get<Note[]>(this.api_path);
  }

  
  getByID(id:number):Observable<Note>{
    return this.http.get<Note>(this.api_path+'/'+id);
  }

  PostData(note:Note):Observable<Note>{
    return this.http.post<Note>(this.api_path,note);
  }

  putByID(note:Note):Observable<Note>{
    return this.http.put<Note>(this.api_path+'/'+note.id,note);
  }

  deleteByID(id:number):Observable<any>{
    return this.http.delete(this.api_path+'/'+id);
  }
}
